use tokio::prelude::*;

use futures::{ Poll, Async, try_ready };

use bytes::Bytes;



pub struct ControllableStdinStream<S> {
    stdin_stream: S,
}

// TODO: change to "impl Stream" when it is available
pub fn controllable_stdin_stream<S>(stdin_stream: S) -> ControllableStdinStream<S> {
    ControllableStdinStream { stdin_stream }
}

impl<S> Stream for ControllableStdinStream<S>
where S: Stream<Item = Bytes> {
    type Item = S::Item;
    type Error = S::Error;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        let result = try_ready!(self.stdin_stream.poll())
            .filter(|bytes| {
                dbg!(bytes);
                bytes != ":exit\r\n"
            });

        Ok(Async::Ready(result))
    }
}
