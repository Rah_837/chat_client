use tokio::{
    prelude::{
        Sink,
        stream::Stream,
        future::Future,
    },
    codec,
    net as anet,
};

use tokio_stdin_stdout as astdio;

mod controllable_stdin_stream;
use controllable_stdin_stream::controllable_stdin_stream;



fn main() {
    let matches = clap::App::new("chat")
        .version("0.1.0")
        .author("Ivan A. <IVAN38562@gmail.com>")
        .about("allows you to instantly exchange messages with other chat users")
        .arg(clap::Arg::with_name("addresses")
            .short("a")
            .multiple(true)
            .takes_value(true)
            .help("address of server")
            .default_value("127.0.0.1:4732"))
        .get_matches();

    let connection = matches
        .values_of("addresses")
        .unwrap()
        .filter_map(|s| s.parse().ok())
        .filter_map(|addr| anet::TcpStream::connect(&addr).wait().ok())
        .next()
        .expect("failed to connect to server");

    let (peer_sink, peer_stream) = {
        let codec = codec::BytesCodec::new();
        let (sink, stream) = codec::Framed::new(connection, codec).split();
        let sink = sink.sink_map_err(|e| eprintln!("failed to write to socket: {}", e));
        let stream = stream
            .map_err(|e| eprintln!("failed to read form socket: {}", e))
            .map(|bytes_mut| bytes::Bytes::from(bytes_mut));

        (sink, stream)
    };


    let controllable_stdin_stream = {
        let stdin = astdio::stdin(0);
        let codec = codec::BytesCodec::new();
        let stdin_stream = codec::FramedRead::new(stdin, codec)
            .map_err(|e| eprintln!("failed to read form stdin: {}", e))
            .map(|bytes_mut| bytes::Bytes::from(bytes_mut));
        
        controllable_stdin_stream(stdin_stream)
    };

    let stdout_sink = {
        let stdout = astdio::stdout(0);
        let codec = codec::BytesCodec::new();
        codec::FramedWrite::new(stdout, codec)
            .sink_map_err(|e| eprintln!("failet to write to stdout: {}", e))
    };


    // TODO: make future that return Async::ready() when recieve a ":exit" message
    let forward_in = peer_stream
        .forward(stdout_sink)
        .map(|_| () );

    let forward_out = controllable_stdin_stream
        .forward(peer_sink)
        .map(|_| () );


    // tokio::run(future::lazy(|| {
    //     tokio::spawn(forward_in);
    //     forward_out
    // }));

    let forward = forward_in
        .select(forward_out)
        .map_err(|_| ())
        .map(|_| ());

   tokio::run(forward);
}




// use tokio::{
//     prelude::{
//         Future,
//         Stream,
//         AsyncRead,
//         AsyncWrite,
//     },
//     net as anet,
// };




// use std::{
//     thread,
//     net::{ TcpStream, SocketAddr },
//     io::{ self, Read, Write },
// };



// fn main() {
//     let (mut peer_reader, mut peer_writer) = {
//         let matches = clap::App::new("chat")
//             .version("0.1.0")
//             .author("Ivan A. <IVAN38562@gmail.com>")
//             .about("allows you to instantly exchange messages with other chat users")
//             .arg(clap::Arg::with_name("addresses")
//                 .short("a")
//                 .multiple(true)
//                 .takes_value(true)
//                 .help("address of server")
//                 .default_value("127.0.0.1:4732"))
//             .get_matches();

//         let reader = matches
//             .values_of("addresses")
//             .unwrap()
//             .filter_map(|s| s.parse().ok())
//             .filter_map(|addr: SocketAddr| TcpStream::connect(&addr).ok())
//             .next()
//             .expect("failed to connect to server");

//         let writer = reader
//             .try_clone()
//             .expect("failed to clone TcpStream");

//         (reader, writer)
//     };

//     let mut out_buffer = [0u8; 1024];
//     let mut in_buffer = [0u8; 1024];

//     thread::spawn(move || {
//         let stdin = io::stdin();
//         let mut lock = stdin.lock();

//         loop {
//             let n = match lock.read(&mut out_buffer) {
//                 Err(e) => {
//                     eprintln!("failed to read from stdin: {}", e);
//                     break;
//                 },
//                 Ok(0) => {
//                     eprintln!("read 0 bytes");
//                     break;
//                 },
//                 Ok(n) => n,
//             };

//             match peer_writer.write(&out_buffer[..n]) {
//                 Err(e) => {
//                     eprintln!("failed to write bytes to socket: {}", e);
//                     break;
//                 },
//                 Ok(0) => {
//                     eprintln!("write 0 bytes");
//                     break;
//                 },
//                 Ok(_) => (),
//             }
//         }
//     });


//     let stdout = io::stdout();
//     let mut lock = stdout.lock();

//     loop {
//         let n = match peer_reader.read(&mut in_buffer) {
//             Err(e) => {
//                 eprintln!("failed to read from socket: {}", e);
//                 break;
//             },
//             Ok(0) => {
//                 eprintln!("read 0 bytes");
//                 break;
//             },
//             Ok(n) => n,
//         };

//         match lock.write_all(&in_buffer[..n]) {
//             Err(e) => {
//                 eprintln!("failed to write bytes to socket: {}", e);
//                 break;
//             },
//             Ok(_) => (),
//         }
//     }
// }
